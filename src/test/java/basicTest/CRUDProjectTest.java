package basicTest;

import io.restassured.response.Response;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

public class CRUDProjectTest {
    @Test
    public void crudProjectRestApi(){

        // Creacion
        JSONObject body= new JSONObject();
        body.put("Content","Maura tarea CRUD");
        body.put("ProjectId",3942189);
        body.put("DueDate","");

        Response response=given()
                .auth()
                .preemptive()
                .basic("mpereyraserrate@gmail.com","maura2021")
                .body(body.toString())
                .log()
                .all()
                .when()
                .post("https://todo.ly/API/Items.json");

        response.then()
                .statusCode(200)
                .body("Content", equalTo("Maura tarea CRUD"))
                .log()
                .all();

        int id = response.then().extract().path("Id");

        // Actualizacion
        body.put("Content","Maura CRUD actualizada");

        response=given()
                .auth()
                .preemptive()
                .basic("mpereyraserrate@gmail.com","maura2021")
                .body(body.toString())
                .log()
                .all()
                .when()
                .put("https://todo.ly/API/Items/"+id+".json");

        response.then()
                .statusCode(200)
                .body("Content", equalTo("Maura CRUD actualizada"))
                .log()
                .all();

        // Get
        response=given()
                .auth()
                .preemptive()
                .basic("mpereyraserrate@gmail.com","maura2021")
                .log()
                .all()
                .when()
                .get("https://todo.ly/API/Items/"+id+".json");

        response.then()
                .statusCode(200)
                .body("Content", equalTo("Maura CRUD actualizada"))
                .log()
                .all();

        // Delete
        response=given()
                .auth()
                .preemptive()
                .basic("mpereyraserrate@gmail.com","maura2021")
                .log()
                .all()
                .when()
                .delete("https://todo.ly/API/Items/"+id+".json");

        response.then()
                .statusCode(200)
                .body("Content", equalTo("Maura CRUD actualizada"))
                .body("Deleted", equalTo(true))
                .log()
                .all();

    }

}
